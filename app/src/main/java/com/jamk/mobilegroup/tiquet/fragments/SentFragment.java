package com.jamk.mobilegroup.tiquet.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jamk.mobilegroup.tiquet.MainActivity;
import com.jamk.mobilegroup.tiquet.R;
import com.jamk.mobilegroup.tiquet.adapters.ReceivedAdapter;
import com.jamk.mobilegroup.tiquet.adapters.SentAdapter;
import com.jamk.mobilegroup.tiquet.model.Receipt;
import com.jamk.mobilegroup.tiquet.repository.ReceiptRepository;

import java.util.List;

import javax.annotation.Nullable;

import io.realm.RealmResults;

/**
 * Created by Aaro on 27.11.2017.
 */

public class SentFragment extends Fragment {

    private ReceiptRepository receiptRepository;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private SentAdapter mAdapter;
    private List<Receipt> receipts;

    public static final String TITLE = "Sent";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        receiptRepository = new ReceiptRepository();
    }

    @Override
    public void onResume() {
        super.onResume();
       // receipts = receiptRepository.RetrieveByStatus(2, MainActivity.User, "creator");

        mAdapter.UpdateAdapter();

    }

    public static SentFragment newInstance() {
        return new SentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //receipts = receiptRepository.RetrieveByStatus(2, MainActivity.User, "creator");

        View v = inflater.inflate(R.layout.fragment_sent, null);

        final RecyclerView text = (RecyclerView) v.findViewById(R.id.rv_sent_receipts);

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Testi", Toast.LENGTH_SHORT).show();
            }
        });

        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_sent_receipts);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new SentAdapter(2, MainActivity.User, "creator");

        mRecyclerView.setAdapter(mAdapter);

        return v;
    }
}
