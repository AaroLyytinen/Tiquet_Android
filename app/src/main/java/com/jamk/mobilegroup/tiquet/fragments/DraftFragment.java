package com.jamk.mobilegroup.tiquet.fragments;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jamk.mobilegroup.tiquet.MainActivity;
import com.jamk.mobilegroup.tiquet.R;
import com.jamk.mobilegroup.tiquet.adapters.DraftAdapter;
import com.jamk.mobilegroup.tiquet.model.Receipt;
import com.jamk.mobilegroup.tiquet.repository.ReceiptRepository;

import android.support.annotation.Nullable;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import io.realm.RealmResults;

/**
 * Created by Aaro on 27.11.2017.
 */

public class DraftFragment extends Fragment {
    public static final String TITLE = "Draft";

    private ReceiptRepository receiptRepository;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private DraftAdapter mAdapter;
    private LayoutInflater mInflater;
    private List<Receipt> receipts;

    public interface DraftFragmentListener {
        void onReceiptClick(Fragment fragment, String id);
    }

    public DraftFragmentListener DFListener;

    public static DraftFragment newInstance() {
        return new DraftFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        receiptRepository = new ReceiptRepository();
    }

    @Override
    public void onResume() {
        super.onResume();

        mAdapter.UpdateAdapter();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            DFListener = (DraftFragmentListener) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement DraftFragmentListener");
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        this.mInflater = inflater;

        receipts = receiptRepository.RetrieveByStatus(1, MainActivity.User, "creator");

        View v = inflater.inflate(R.layout.fragment_draft, null);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_draft_receipts);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new DraftAdapter(1, MainActivity.User, "creator");

        mAdapter.setOnItemClickListener(new DraftAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String id) {
                DFListener.onReceiptClick(DraftFragment.this, id);
            }
        });

        mRecyclerView.setAdapter(mAdapter);

        return v;
    }
    public void Update(){
        mAdapter.UpdateAdapter();
    }
}
