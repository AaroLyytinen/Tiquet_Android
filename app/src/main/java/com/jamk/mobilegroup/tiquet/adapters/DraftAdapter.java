package com.jamk.mobilegroup.tiquet.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jamk.mobilegroup.tiquet.R;
import com.jamk.mobilegroup.tiquet.model.Receipt;
import com.jamk.mobilegroup.tiquet.repository.ReceiptRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.realm.RealmResults;

import static io.realm.internal.SyncObjectServerFacade.getApplicationContext;

/**
 * Created by Aaro on 27.11.2017.
 */

public class DraftAdapter extends RecyclerView.Adapter<DraftAdapter.ViewHolder> {
    private OnItemClickListener listener;
    private Context context;
    private List<Receipt> mReceipts;
    private ReceiptRepository receiptRepository;
    private int sts;
    private String user, key;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvProduct, tvManufacturer, tvModel, tvSerial, tvDescription, tvPrice, tvCreator, tvCreatetime;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            tvProduct = itemView.findViewById(R.id.tv_card_receipt_product);
            tvManufacturer = itemView.findViewById(R.id.tv_card_receipt_manufacturer);
            tvModel = itemView.findViewById(R.id.tv_card_receipt_model);
            tvSerial = itemView.findViewById(R.id.tv_card_receipt_serial);
            tvDescription = itemView.findViewById(R.id.tv_card_receipt_description);
            tvPrice = itemView.findViewById(R.id.tv_card_receipt_price);
            tvCreator = itemView.findViewById(R.id.tv_card_receipt_creator);
            tvCreatetime = itemView.findViewById(R.id.tv_card_receipt_createtime);
        }

        @Override
        public void onClick(View view) {
            Receipt receipt = mReceipts.get(getAdapterPosition());
            listener.onItemClick(receipt.getId());
        }
    }

    public DraftAdapter(int sts, String user, String key) {
        this.sts = sts;
        this.user = user;
        this.key = key;
        receiptRepository = new ReceiptRepository();
        this.mReceipts = receiptRepository.RetrieveByStatus(sts, user, key);
        context = getApplicationContext();
    }

    public void UpdateAdapter() {
        this.mReceipts = receiptRepository.RetrieveByStatus(sts, user, key);
        this.notifyDataSetChanged();
    }

    @Override
    public DraftAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_receipt, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String strProduct, strManufacturer, strModel, strSerial, strDescription, strPrice, strCreator, strCreatetime;
        try {
            JSONObject json = new JSONObject(mReceipts.get(position).getReceipt());
            strProduct = context.getString(R.string.card_txt_product) + ": " + json.get("product");
            holder.tvProduct.setText(strProduct);
            strManufacturer = context.getString(R.string.card_txt_manufacturer) + ": " + json.get("manufacturer");
            holder.tvManufacturer.setText(strManufacturer);
            strModel = context.getString(R.string.card_txt_model) + ": " + json.get("model");
            holder.tvModel.setText(strModel);
            strSerial = context.getString(R.string.card_txt_serial) + ": " + json.get("serial");
            holder.tvSerial.setText(strSerial);
            strDescription = context.getString(R.string.card_txt_description) + ": " + json.get("description");
            holder.tvDescription.setText(strDescription);
            strPrice = context.getString(R.string.card_txt_price) + ": " + json.get("price");
            holder.tvPrice.setText(strPrice);
            strCreator = context.getString(R.string.card_txt_creator) + ": " + json.get("creator");
            holder.tvCreator.setText(strCreator);
            strCreatetime = "Tap to send";
            holder.tvCreatetime.setText(strCreatetime);

        }
        catch (JSONException e) { }
    }

    @Override
    public int getItemCount(){
        return mReceipts.size();
    }

    public interface OnItemClickListener{
        void onItemClick(String id);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
