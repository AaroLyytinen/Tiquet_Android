package com.jamk.mobilegroup.tiquet.repository;

import android.app.AlertDialog;
import android.os.Build;
import android.speech.RecognizerIntent;

import com.jamk.mobilegroup.tiquet.model.Receipt;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Veeti on 19.11.2017.
 */

public class ReceiptRepository {
    private Realm realm;
    private String user;
    private UserRepository userRepository;

    public ReceiptRepository() {
        userRepository = new UserRepository();
    }

    public void AddToRealm (final String object, final int status) {
        realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                UUID id = UUID.randomUUID();
                Receipt dbReceipt = realm.createObject(Receipt.class, id.toString());
                dbReceipt.setReceipt(object);
                dbReceipt.setStatus(status);
            }
        });
    }

    public void Delete (Receipt receipt) {
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        receipt.deleteFromRealm();
        realm.commitTransaction();
    }

    public void DeleteById (String id) {
        realm = Realm.getDefaultInstance();
        RealmResults<Receipt> receipt = FindById(id);
        Delete(receipt.first());
    }

    public RealmResults<Receipt> RetrieveAll() {
        realm = Realm.getDefaultInstance();
        return realm.where(Receipt.class).findAll();
    }

    public List<Receipt> RetrieveByStatus(int sts, String user, String key) {
        List<Receipt> receipts = new ArrayList<>();
        user = userRepository.GetNameByID(user);

        realm = Realm.getDefaultInstance();
        RealmQuery<Receipt> query = realm.where(Receipt.class);
        query.equalTo("status", sts);

        RealmResults<Receipt> results = query.findAll();
        for (int i = 0; i<results.size(); i++) {
            try {
                JSONObject json = new JSONObject(results.get(i).getReceipt().toString());
                if (json.get(key).equals(user)) {
                    receipts.add(results.get(i));
                }
            }
            catch (JSONException e) {

            }
        }
        return receipts;
    }

    public List<Receipt> RetrieveByUser(int sts, String user) {
        List<Receipt> receipts = new ArrayList<>();
        user = userRepository.GetNameByID(user);

        realm = Realm.getDefaultInstance();
        RealmQuery<Receipt> query = realm.where(Receipt.class);
        query.equalTo("status", sts);

        RealmResults<Receipt> results = query.findAll();
        for (int i = 0; i<results.size(); i++) {
            try {
                JSONObject json = new JSONObject(results.get(i).getReceipt().toString());
                if(json.get("user").equals(user)) {
                    receipts.add(results.get(i));
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return receipts;
    }

    public RealmResults<Receipt> FindById(String id) {
        realm = Realm.getDefaultInstance();
        RealmQuery<Receipt> query = realm.where(Receipt.class);
        query.equalTo("id", id);

        RealmResults<Receipt> result = query.findAll();
        return result;
    }

    public RealmResults<Receipt> FindByReceipt(String receipt) {
        realm = Realm.getDefaultInstance();
        RealmQuery<Receipt> query = realm.where(Receipt.class);
        query.equalTo("receipt", receipt);

        RealmResults<Receipt> result = query.findAll();
        return result;
    }

    public void UpdateFields(final Receipt receipt, final String strReceipt) {
        realm = Realm.getDefaultInstance();
        realm.refresh();
        RealmResults<Receipt> result = FindById(receipt.getId());
        //Delete(result.first());

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                UUID id = UUID.randomUUID();
                Receipt dbReceipt = realm.createObject(Receipt.class, id.toString());
                dbReceipt.setReceipt(strReceipt);
                dbReceipt.setStatus(2);
            }
        });
    }
}
