package com.jamk.mobilegroup.tiquet.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Veeti on 10.11.2017.
 */

public class Receipt extends RealmObject {
    @PrimaryKey
    private String id;
    @Required
    private String receipt;

    private int status;

    public Receipt(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceipt() {
        return receipt;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
