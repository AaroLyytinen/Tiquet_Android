package com.jamk.mobilegroup.tiquet.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;

import com.jamk.mobilegroup.tiquet.R;
import com.jamk.mobilegroup.tiquet.model.User;
import com.jamk.mobilegroup.tiquet.repository.UserRepository;

import io.realm.RealmObject;

/**
 * Created by Aaro on 24.11.2017.
 */

public class CreateUserDialogFragment extends DialogFragment {
    EditText mEditName, mEditUsername, mEditPassword, mEditAge;
    private UserRepository userRepository;


    public interface RegisterDialogListener {
        void doPositiveClick(DialogFragment dialog, RealmObject user);
        void doNegativeClick(DialogFragment dialog);
    }
    RegisterDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (RegisterDialogListener) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ReceiptDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.dialog_register_form, null))
                .setPositiveButton("Register", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int i) {

                        Dialog f = (Dialog) dialog;

                        mEditName = (EditText) f.findViewById(R.id.NameRegister);
                        mEditUsername = (EditText) f.findViewById(R.id.UsernameLogin);
                        mEditPassword = (EditText) f.findViewById(R.id.PasswordRegister);
                        mEditAge = (EditText) f.findViewById(R.id.AgeRegister);

                        userRepository = new UserRepository();

                        User user = new User();
                        user.setName(mEditName.getText().toString());
                        user.setUsername(mEditUsername.getText().toString());
                        user.setPassword(mEditPassword.getText().toString());
                        user.setEmail(mEditAge.getText().toString());
                        userRepository.AddToRealm(user);
                        mListener.doPositiveClick(CreateUserDialogFragment.this, user);
                    }
                })
                .setNegativeButton(R.string.create_form_cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                CreateUserDialogFragment.this.getDialog().cancel();
                                mListener.doNegativeClick(CreateUserDialogFragment.this);
                            }
                        });
        return builder.create();

    }


}
