package com.jamk.mobilegroup.tiquet;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.jamk.mobilegroup.tiquet.adapters.ViewPagerAdapter;
import com.jamk.mobilegroup.tiquet.fragments.CreateReceiptDialogFragment;
import com.jamk.mobilegroup.tiquet.fragments.DraftFragment;
import com.jamk.mobilegroup.tiquet.model.Receipt;
import com.jamk.mobilegroup.tiquet.repository.ReceiptRepository;
import com.jamk.mobilegroup.tiquet.repository.UserRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.Date;

import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity implements
        CreateReceiptDialogFragment.ReceiptDialogListener, NfcAdapter.CreateNdefMessageCallback,
        DraftFragment.DraftFragmentListener{
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private NfcAdapter mNfcAdapter;
    private String strReceipt;
    private ReceiptRepository receiptRepository;
    private UserRepository userRepository;
    private ViewPager mViewPager;
    private Toolbar mToolBar;
    private ViewPagerAdapter mViewPagerAdapter;
    private TabLayout mTabLayout;
    private String user;
    public static DraftFragment draftFragment;
    public static String User;
    public static final String RECEIPT_ID = "com.jamk.mobilegroup.tiquet.ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user = ReadFromFile(getApplicationContext());
        draftFragment = DraftFragment.newInstance();

        User = user;

        mToolBar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(mToolBar);

        receiptRepository = new ReceiptRepository();
        userRepository = new UserRepository();
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        setBeamCompleteCallback(this, new NfcAdapter.OnNdefPushCompleteCallback() {
            @Override
            public void onNdefPushComplete(NfcEvent event) {
                NfcCompleted();
            }
        });

        setViewPager();
    }

    private String ReadFromFile(Context context) {
        String ret = "";
        try {
            InputStream inputStream = context.openFileInput("logged_user.txt");
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString=bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkNfc();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())){
            processIntent(getIntent());
            getIntent().setAction("NFC_READ");
        }
        //mViewPagerAdapter.notifyDataSetChanged();
        /*
        if(mViewPager != null && mViewPager.getAdapter() != null) {
            mViewPager.getAdapter().notifyDataSetChanged();
        }*/
    }

    public void NfcCompleted() {
        /*
        RealmResults<Receipt> result = receiptRepository.FindByReceipt(strReceipt);
        receiptRepository.UpdateFields(result.get(0));
        Toast.makeText(getApplicationContext(), "Status set to 2", Toast.LENGTH_SHORT).show();
        */
    }

    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    public void setBeamCompleteCallback(Activity activity, NfcAdapter.OnNdefPushCompleteCallback callback) {
        mNfcAdapter.setOnNdefPushCompleteCallback(callback, activity);

    }

    private void checkNfc() {
        if (mNfcAdapter != null && !mNfcAdapter.isEnabled()) {
            Toast.makeText(getApplicationContext(), "NFC is disabled!", Toast.LENGTH_LONG).show();
        }
    }

    public void openCreateReceiptDialog(View view) {
        Bundle args = new Bundle();
        args.putString("user", user);

        CreateReceiptDialogFragment dialog = new CreateReceiptDialogFragment();
        dialog.setArguments(args);
        dialog.show(getFragmentManager(), "openDialog");
    }

    @Override
    public void doPositiveClick(DialogFragment dialog, JSONObject receipt) {
        strReceipt = receipt.toString();

        Toast.makeText(getApplicationContext(), "Ndef message set", Toast.LENGTH_LONG).show();

        receiptRepository.AddToRealm(strReceipt, 1);
        draftFragment.Update();
    }

    @Override
    public void doNegativeClick(DialogFragment dialog) {
        Toast.makeText(getApplicationContext(), "Cancel clicked", Toast.LENGTH_LONG).show();
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        RealmResults<Receipt> result = receiptRepository.FindByReceipt(strReceipt);
        try {
            JSONObject receipt = new JSONObject(strReceipt);
            String currentTime = DateFormat.getDateTimeInstance().format(new Date());
            receipt.put("createtime",currentTime);
            strReceipt = receipt.toString();
        }
        catch (JSONException e) {

        }
        receiptRepository.UpdateFields(result.get(0), strReceipt);

        return new NdefMessage(new NdefRecord[]{
                NdefRecord.createMime(
                        "application/com.jamk.mobilegroup.tiquet", strReceipt.getBytes())
        });
    }

    private void processIntent(Intent intent) {
        Parcelable[] rawMessages = intent.getParcelableArrayExtra(
                NfcAdapter.EXTRA_NDEF_MESSAGES);
        NdefMessage message = (NdefMessage) rawMessages[0];
        String receipt = new String(message.getRecords()[0].getPayload());
        try {
            JSONObject jsonReceipt = new JSONObject(receipt);
            jsonReceipt.put("user", userRepository.GetNameByID(User));
            receiptRepository.AddToRealm(jsonReceipt.toString(), 3);
        }
        catch (JSONException e) {
            Toast.makeText(getApplicationContext(), "Error while receiving NDEF payload", Toast.LENGTH_LONG).show();
        }
    }

    private void setViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mViewPagerAdapter.setUser(user);
        mViewPager.setAdapter(mViewPagerAdapter);

        mTabLayout = (TabLayout) findViewById(R.id.tab);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void onReceiptClick(Fragment fragment, String id) {

        Intent intent = new Intent(this, TransferActivity.class);
        intent.putExtra(RECEIPT_ID, id);
        startActivity(intent);

        /*
        RealmResults<Receipt> receipts = receiptRepository.FindById(id);
        strReceipt = receipts.first().getReceipt().toString();
        mNfcAdapter.setNdefPushMessageCallback(this, this);
        */
    }
}
