package com.jamk.mobilegroup.tiquet.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jamk.mobilegroup.tiquet.R;
import com.jamk.mobilegroup.tiquet.model.Receipt;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.realm.RealmResults;

import static io.realm.internal.SyncObjectServerFacade.getApplicationContext;

/**
 * Created by Aaro on 27.11.2017.
 */

public class ReceivedAdapter extends RecyclerView.Adapter<ReceivedAdapter.ViewHolder> {
    private Context context;
    private List<Receipt> mReceipts;

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvProduct, tvManufacturer, tvModel, tvSerial, tvDescription, tvPrice, tvCreator, tvCreatetime;

        public ViewHolder(View itemView) {
            super(itemView);

            tvProduct = itemView.findViewById(R.id.tv_card_receipt_product);
            tvManufacturer = itemView.findViewById(R.id.tv_card_receipt_manufacturer);
            tvModel = itemView.findViewById(R.id.tv_card_receipt_model);
            tvSerial = itemView.findViewById(R.id.tv_card_receipt_serial);
            tvDescription = itemView.findViewById(R.id.tv_card_receipt_description);
            tvPrice = itemView.findViewById(R.id.tv_card_receipt_price);
            tvCreator = itemView.findViewById(R.id.tv_card_receipt_creator);
            tvCreatetime = itemView.findViewById(R.id.tv_card_receipt_createtime);
        }
    }

    public ReceivedAdapter(List<Receipt> receipts) {
        this.mReceipts = receipts;
        context = getApplicationContext();
    }

    @Override
    public ReceivedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_receipt, parent, false);

        return new ReceivedAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReceivedAdapter.ViewHolder holder, final int position) {
        String strProduct, strManufacturer, strModel, strSerial, strDescription, strPrice, strCreator, strCreatetime;
        try {
            JSONObject json = new JSONObject(mReceipts.get(position).getReceipt());
            strProduct = context.getString(R.string.card_txt_product) + ": " + json.get("product");
            holder.tvProduct.setText(strProduct);
            strManufacturer = context.getString(R.string.card_txt_manufacturer) + ": " + json.get("manufacturer");
            holder.tvManufacturer.setText(strManufacturer);
            strModel = context.getString(R.string.card_txt_model) + ": " + json.get("model");
            holder.tvModel.setText(strModel);
            strSerial = context.getString(R.string.card_txt_serial) + ": " + json.get("serial");
            holder.tvSerial.setText(strSerial);
            strDescription = context.getString(R.string.card_txt_description) + ": " + json.get("description");
            holder.tvDescription.setText(strDescription);
            strPrice = context.getString(R.string.card_txt_price) + ": " + json.get("price");
            holder.tvPrice.setText(strPrice);
            strCreator = context.getString(R.string.card_txt_creator) + ": " + json.get("creator");
            holder.tvCreator.setText(strCreator);
            strCreatetime = context.getString(R.string.card_txt_createtime) + ": " + json.get("createtime");
            holder.tvCreatetime.setText(strCreatetime);

        }
        catch (JSONException e) { }
    }

    @Override
    public int getItemCount(){
        return mReceipts.size();
    }
}
