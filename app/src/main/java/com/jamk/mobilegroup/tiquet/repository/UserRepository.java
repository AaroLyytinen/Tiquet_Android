package com.jamk.mobilegroup.tiquet.repository;

import com.jamk.mobilegroup.tiquet.model.User;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Aaro on 27.11.2017.
 */

public class UserRepository {
    private Realm realm;

    public UserRepository() { }

    public void AddToRealm (final User object){
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                UUID id = UUID.randomUUID();
                User dbuser = realm.createObject(User.class, id.toString());
                dbuser.setEmail(object.getEmail());
                dbuser.setName(object.getName());
                dbuser.setUsername(object.getUsername());
                dbuser.setPassword(object.getUsername());
            }
        });

    }

    public String GetNameByID (String id) {
        realm = Realm.getDefaultInstance();
        RealmQuery<User> query = realm.where(User.class);
        query.equalTo("id", id);
        RealmResults<User> result = query.findAll();
        return result.first().getName().toString();
    }

    public String CheckUser(String username, String password){
        realm = Realm.getDefaultInstance();
        RealmQuery<User> query = realm.where(User.class);
        query.beginGroup()
                .equalTo("username", username)
                .equalTo("password", password)
                .endGroup();

        RealmResults<User> result = query.findAll();
        if(!result.isEmpty())
        {
            return result.first().getId().toString();
        }
        else {
            return null;
        }
    }
}
