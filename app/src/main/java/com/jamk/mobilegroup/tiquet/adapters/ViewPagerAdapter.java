package com.jamk.mobilegroup.tiquet.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.jamk.mobilegroup.tiquet.MainActivity;
import com.jamk.mobilegroup.tiquet.fragments.DraftFragment;
import com.jamk.mobilegroup.tiquet.fragments.ReceivedFragment;
import com.jamk.mobilegroup.tiquet.fragments.SentFragment;

/**
 * Created by Aaro on 27.11.2017.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private String user;
    private static int TAB_COUNT = 3;

    public ViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ReceivedFragment.newInstance();
            case 1:
                return SentFragment.newInstance();
            case 2:
                return MainActivity.draftFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ReceivedFragment.TITLE;
            case 1:
                return SentFragment.TITLE;
            case 2:
                return DraftFragment.TITLE;
        }
        return super.getPageTitle(position);
    }

    public void setUser(String user) {
        this.user = user;
    }
}
